import React from 'react';
import ReactDOM from 'react-dom';
import ReactDOMServer from 'react-dom/server';
import { Promise } from 'nui-utils';

export default (NeueUI) => {
	var PageStructPackage;
	NeueUI.get('views$base:PageStructure').then(view => PageStructPackage = view);

	return NeueUI.Dispatcher.when('nui.router.renderable').then(PagePkg => {
		NeueUI.log('Rendering...');

		const PageStructure = PageStructPackage.Component;

		const { Component, ref } = PagePkg;
		const props = PagePkg.props || {};

		return {
			ref: ref,
			Component: Component,
			text: (
				ReactDOMServer.renderToStaticMarkup(
					<PageStructure>
						{ReactDOMServer.renderToString(<Component {...props} />)}
					</PageStructure>
				)
			)
		};
	});
}
